### Technologies

* React
* Node 4.1.1
* Express
* EJS
* jQuery

### Running the application

```
node app.js
```

### Design Decisions

I took advantage of server-side rendering with React. React makes it easy to render both server-side and client-side rendering using the same "template", unlike with other JS libraries/frameworks. I abstracted away the YouTube API searching to the playlist module for reuse. For the App component, I made it dependent on a mixin `PlaylistsMixin` which fetches the data using AJAX via jQuery.
