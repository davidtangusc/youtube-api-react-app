require('node-jsx').install({ extension: '.jsx' });

var express = require('express');
var React = require('react/addons');
var components = require('./public/components.jsx');
var playlist = require('./app/playlist');

var app = express();
var App = React.createFactory(components.App);

app.use(express.static(__dirname + '/public'));
app.set('view engine', 'ejs');

app.get('/', function(req, res) {
	playlist.query('Elton John').then(function(playlists) {
		var data = {
			artists: [
				'Elton John',
				'Stevie Wonder',
				'Frank Sinatra',
				'Louis Armstrong'
			],
			playlists: playlists
		};

		res.render('index', {
			react: React.renderToString(App(data)),
			data: JSON.stringify(data)
		});
	});
});

app.get('/api/playlists', function(req, res) {
	playlist.query(req.query.artist).then(function(playlists) {
		res.json(playlists);
	});
});


app.listen(3000, function() {
	console.log('Listening on port 3000');
});
