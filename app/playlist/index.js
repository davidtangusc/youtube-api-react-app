var request = require('request');
var qs = require('qs');
var Q = require('q');
var config = require('./../../config/youtube');

var baseUrl = 'https://www.googleapis.com/youtube/v3/search';

module.exports = {
	query: function(q) {
		var deferred = Q.defer();
		var url = baseUrl + '?' + qs.stringify({
			part: 'snippet',
			q: q,
			key: config.key,
			type: 'playlist'
		});

		console.log(url);

		request(url, function(error, response, body) {
		  if (!error && response.statusCode == 200) {
		  	var items = JSON.parse(body).items;
		    deferred.resolve(items);
		  }
		});

		return deferred.promise;
	}
};
