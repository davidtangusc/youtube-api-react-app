(function() {

var isNode = typeof module !== 'undefined' && module.exports;
var React = isNode ? require('react/addons') : window.React;

var PlaylistsMixin = {
	getPlaylistsForArtist: function(artist) {
		return $.ajax({
			url: '/api/playlists',
			data: {
				artist: artist
			}
		});
	}
};


var App = React.createClass({
	fetchArtistPlaylist: function(artist) {
		this.getPlaylistsForArtist(artist).then(function(playlists) {
			this.setState({
				artist: artist,
				playlists: playlists
			});
		}.bind(this));
	},

	mixins: [PlaylistsMixin],

	propTypes: {
		playlists: React.PropTypes.array.isRequired,
		artists: React.PropTypes.array.isRequired
	},

	getInitialState: function() {
		return {
			artists: this.props.artists,
			playlists: this.props.playlists
		};
	},

	render: function() {
		return (
			<div>
				<ArtistSelect artists={this.state.artists} onChange={this.fetchArtistPlaylist} />
				<PlaylistList playlists={this.state.playlists} />
			</div>
		);
	}
});

var ArtistSelect = React.createClass({
	propTypes: {
		artists: React.PropTypes.array.isRequired,
		onChange: React.PropTypes.func.isRequired
	},

	handleChange: function() {
		this.props.onChange(this.refs.artist.getDOMNode().value);
	},

	render: function() {
		var options = this.props.artists.map(function(artist) {
			return <option value={artist}>{artist}</option>;
		});

		return (
			<select onChange={this.handleChange} ref="artist">{options}</select>
		);
	}
});

var PlaylistList = React.createClass({
	propTypes: {
		playlists: React.PropTypes.array.isRequired
	},

	render: function() {
		var playlists = this.props.playlists.map(function(playlist) {
			return <Playlist playlist={playlist} />;
		}, this);

		return <div className="playlists">{playlists}</div>;
	}
});

var Playlist = React.createClass({
	propTypes: {
		playlist: React.PropTypes.object.isRequired
	},

	render: function() {
		var src = "https://www.youtube.com/embed/videoseries?list=" + this.props.playlist.id.playlistId;
		var desc = 'No description provided.';

		if (this.props.playlist.snippet.description) {
			desc = this.props.playlist.snippet.description;
		}

		return (
			<div className="playlist clearfix">
				<div className="playlist-details">
					<h4>{this.props.playlist.snippet.title}</h4>
					<p>{desc}</p>
				</div>
				<iframe width="560" height="315" src={src} frameBorder="0" allowFullScreen></iframe>
			</div>
		);
	}
});

if (isNode) {
  exports.App = App;
} else {
  React.render(
		<App artists={window.data.artists} playlists={window.data.playlists} />,
		document.getElementById('react-root')
	);
}

})();
